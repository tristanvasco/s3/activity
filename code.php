<?php 

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	// Constructor

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName() {
		return "your full name is $this->firstName $this->lastName";
	}
};

// Instances
$person = new Person('Senku', 'S.', 'Ishigami');

class Developer extends Person {

	public function printName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
	}

};

$developer = new Developer('John', 'Finch', 'Smith');

class Engineer extends Person {

	public function printName() {
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName";
	}

};

$engineer = new Engineer('Harold', 'Myers', 'Reese');

// class Condominium extends Building {

// 	public function printName() {
// 		return "The name of the condominium is $this->name";
// 	}
// 	// getters(read-only) & setters(write-only)
// 	public function getName() {
// 		return $this->name;
// 	}

// 	public function setName($name){
// 		$this->name = $name;
// 	}
// };






 ?>